package com.m174k4.atmap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.BulgarianBorders;
import com.m174k4.atmap.parser.*;
import com.m174k4.atmap.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/*
 * Created by Dimitar Ralev on 29.8.2016.
 */
class Parser {
	void doStuff() {
		List<ATM> atms = parse(false);
		saveEncryptedFile(atms);
		saveFormattedFile(atms);
	}

	private Callable<List<ATM>> callable1 = () -> new AllianzParser().parse(false);
	private Callable<List<ATM>> callable2 = () -> new CcbParser().parse(false);
	private Callable<List<ATM>> callable3 = () -> new CibankParser().parse(false);
	private Callable<List<ATM>> callable4 = () -> new DBankParser().parse(false);
	private Callable<List<ATM>> callable5 = () -> new DskParser().parse(false);
	private Callable<List<ATM>> callable6 = () -> new FibParser().parse(false);
	private Callable<List<ATM>> callable7 = () -> new IabParser().parse(false);
	private Callable<List<ATM>> callable8 = () -> new IbankParser().parse(false);
	private Callable<List<ATM>> callable9 = () -> new MunicipalbankParser().parse(false);
	private Callable<List<ATM>> callable10 = () -> new PiraeusParser().parse(false);
	private Callable<List<ATM>> callable11 = () -> new PostbankParser().parse(false);
	private Callable<List<ATM>> callable12 = () -> new ProcreditParser().parse(false);
	private Callable<List<ATM>> callable13 = () -> new RaifaizenParser().parse(false);
	private Callable<List<ATM>> callable14 = () -> new SgebParser().parse(false);
	private Callable<List<ATM>> callable15 = () -> new TokudaParser().parse(false);
	private Callable<List<ATM>> callable16 = () -> new UbbParser().parse(false);
	private Callable<List<ATM>> callable17 = () -> new UnicreditParser().parse(false);

	private List<ATM> parse(boolean isFull) {
		List<Callable<List<ATM>>> taskList = new ArrayList<>();
		taskList.add(callable1);
		taskList.add(callable2);
		taskList.add(callable3);
		taskList.add(callable4);
		taskList.add(callable5);
		taskList.add(callable6);
		taskList.add(callable7);
		taskList.add(callable8);
		taskList.add(callable9);
		taskList.add(callable10);
		taskList.add(callable11);
		taskList.add(callable12);
		taskList.add(callable13);
		taskList.add(callable14);
		taskList.add(callable15);
		taskList.add(callable16);
		taskList.add(callable17);

		List<ATM> all = new ArrayList<>();
		try {
			ExecutorService executor = Executors.newFixedThreadPool(10);
			List<Future<List<ATM>>> list = executor.invokeAll(taskList);
			for (Future<List<ATM>> future : list) {
				addToList(all, future.get());
			}
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}

		all = new ArrayList<>(removeOutsideOfBorders(removeDuplicates(all)));

		return all;
	}

	private void addToList(List<ATM> list, List<ATM> listToAdd) {
		if (listToAdd != null) {
			list.addAll(listToAdd);
		}
	}

	private List<ATM> removeDuplicates(List<ATM> atms) {
		boolean toAdd;
		List<ATM> filteredAtms = new ArrayList<>();
		for (ATM atm : atms) {
			toAdd = true;
			for (ATM atmInner : filteredAtms) {
				if (atm.getLat() == atmInner.getLat() && atm.getLon() == atmInner.getLon() && atm.getBank() == atmInner.getBank()) {
					toAdd = false;
				}
			}
			if (toAdd)
				filteredAtms.add(atm);
		}
		return filteredAtms;
	}

	private List<ATM> removeOutsideOfBorders(List<ATM> atms) {
		atms.removeIf(atm -> atm.getLat() < BulgarianBorders.getLatMin()
				|| atm.getLat() > BulgarianBorders.getLatMax()
				|| atm.getLon() < BulgarianBorders.getLonMin()
				|| atm.getLon() > BulgarianBorders.getLonMax());
		return atms;
	}

	private void saveEncryptedFile(List<ATM> atms) {
		System.out.println("Saving to encrypted file..");
		FileUtils.writeEncryptedFile(new Gson().toJson(atms));
	}

	private void saveFormattedFile(List<ATM> atms) {
		System.out.println("Saving to formatted file..");

		String jsonString = new Gson().toJson(atms);

		JsonParser parser = new JsonParser();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		JsonElement el = parser.parse(jsonString);
		jsonString = gson.toJson(el);

		FileUtils.writeFile(jsonString);
	}

	private String clearFormatting(String str) {
		str = str.replaceAll("\t", "");
		str = str.replaceAll("\n", "");
		str = str.replaceAll(" ", "");

		return str;
	}
}
