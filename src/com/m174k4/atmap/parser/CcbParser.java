package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.bank.CcbObject;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 16.8.2016 г..
 */
public class CcbParser extends ParserBase {
	private static final String URL = "http://www.ccbank.bg/bonuses/storelocations/?lang=bg&callback=jQuery110205718729516815826_1471020488650&_=1471020488656";
	private static final String DIR = "json";
	private static final String BANK_NAME = "Ccb";
	private static final String FILE_NAME = "ccb.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		int start = "jQuery110205718729516815826_1471020488650(".length();
		int end = str.length()-2;
		str = str.substring(start, end);
		str = "{\"offices\":" + str + "}";
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, CcbObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		CcbObject.Offices[] bankAtms = ((CcbObject) bankObject).getOffices();
		for (CcbObject.Offices bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.CCB);
			atm.setAddress(bankAtm.getAddress());
			atm.setLat(Double.parseDouble(bankAtm.getLatitude()));
			atm.setLon(Double.parseDouble(bankAtm.getLongitude()));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		CcbObject.Offices[] bankAtms = ((CcbObject) bankObject).getOffices();
		for (CcbObject.Offices bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.CCB);
			atm.setLat(Double.parseDouble(bankAtm.getLatitude()));
			atm.setLon(Double.parseDouble(bankAtm.getLongitude()));
			atms.add(atm);
		}
		return atms;
	}
}
