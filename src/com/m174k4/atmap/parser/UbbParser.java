package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.bank.UBBObject;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 16.8.2016 г..
 */
public class UbbParser extends ParserBase {
	private static final String URL = "https://www.ubb.bg/offices/pins?city_id=&address=&type=atms&_=1511535517035";
	private static final String DIR = "json";
	private static final String BANK_NAME = "Ubb";
	private static final String FILE_NAME = "ubb.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, UBBObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		UBBObject.Atms[] bankAtms = ((UBBObject) bankObject).getMarkers().getAtms();
		for (UBBObject.Atms ubbAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.UBB);
			atm.setCity(ubbAtm.getData().getCity());
			atm.setLat(Double.parseDouble(ubbAtm.getData().getLat()));
			atm.setLon(Double.parseDouble(ubbAtm.getData().getLng()));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		UBBObject.Atms[] bankAtms = ((UBBObject) bankObject).getMarkers().getAtms();
		for (UBBObject.Atms ubbAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.UBB);
			atm.setLat(Double.parseDouble(ubbAtm.getData().getLat()));
			atm.setLon(Double.parseDouble(ubbAtm.getData().getLng()));
			atms.add(atm);
		}
		return atms;
	}
}
