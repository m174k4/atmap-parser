package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.Banks;
import com.m174k4.atmap.model.bank.DBankObject;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 16.8.2016.
 */
public class DBankParser extends ParserBase {
	private static final String URL = "https://www.dbank.bg/bg/klonove-i-bankomati";
	private static final String DIR = "kml";
	private static final String BANK_NAME = "DBank";
	private static final String FILE_NAME = "dbank.kml";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
//		str = str.replaceAll("\n", "");  // removes new lines
		str = str.replaceAll(" {2}", "");  // removes double spaces
		int start = str.indexOf("var dataBranches = ") + 19;
		int end = str.indexOf("},];", start) + 1;
		str = str.substring(start, end);
		str = str + "]";
		str = "{\"offices\":" + str + "}";

		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, DBankObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		DBankObject.Offices[] cities = ((DBankObject) bankObject).getOffices();
		for (DBankObject.Offices city : cities) {
			for (DBankObject.City_branches branch : city.getCity_branches()) {
				ATMFull atm = new ATMFull();
				atm.setBank(Banks.DBANK);
				atm.setAddress(branch.getName());
				int index = branch.getLocation().indexOf(',');
				String lat = branch.getLocation().substring(0, index);
				String lon = branch.getLocation().substring(index + 1, branch.getLocation().length());
				atm.setLat(Double.parseDouble(lat));
				atm.setLon(Double.parseDouble(lon));
				atms.add(atm);
			}
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		DBankObject.Offices[] cities = ((DBankObject) bankObject).getOffices();
		for (DBankObject.Offices city : cities) {
			for (DBankObject.City_branches branch : city.getCity_branches()) {
				ATMFull atm = new ATMFull();
				atm.setBank(Banks.DBANK);
				int index = branch.getLocation().indexOf(',');
				String lat = branch.getLocation().substring(0, index);
				String lon = branch.getLocation().substring(index + 1, branch.getLocation().length());
				atm.setLat(Double.parseDouble(lat));
				atm.setLon(Double.parseDouble(lon));
				atms.add(atm);
			}
		}
		return atms;
	}
}
