package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.bank.DskObject;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 17.8.2016.
 */
public class DskParser extends ParserBase {
	private static final String URL = "https://dskbank.bg/%D0%BA%D0%BE%D0%BD%D1%82%D0%B0%D0%BA%D1%82%D0%B8/%D0%BA%D0%BB%D0%BE%D0%BD%D0%BE%D0%B2%D0%B0-%D0%BC%D1%80%D0%B5%D0%B6%D0%B0/GetOffices/?cityId=&regionId=&branchType=Atm&service=&latitude=&longit";
	private static final String DIR = "other";
	private static final String BANK_NAME = "Dsk";
	private static final String FILE_NAME = "dsk.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		str = "{\"offices\":" + str + "}";
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, DskObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		DskObject.Offices[] bankAtms = ((DskObject) bankObject).getOffices();
		for (DskObject.Offices bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.DSK);
			atm.setCity(bankAtm.getCity());
			atm.setLat(Double.parseDouble(bankAtm.getLatitude()));
			atm.setLon(Double.parseDouble(bankAtm.getLongitude()));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		DskObject.Offices[] bankAtms = ((DskObject) bankObject).getOffices();
		for (DskObject.Offices bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.DSK);
			atm.setLat(Double.parseDouble(bankAtm.getLatitude()));
			atm.setLon(Double.parseDouble(bankAtm.getLongitude()));
			atms.add(atm);
		}
		return atms;
	}
}
