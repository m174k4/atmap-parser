package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.bank.ProcreditObject;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 17.8.2016.
 */
public class ProcreditParser extends ParserBase {
	private static final String URL = "http://www.procreditbank.bg/bg/page/8/order/title_asc/filters/182";
	private static final String DIR = "other";
	private static final String BANK_NAME = "Procredit";
	private static final String FILE_NAME = "procredit.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		int start = str.indexOf("boundsAll = ");
		int end = str.indexOf("widget.map.panToBounds(boundsAll);", start);
		str = str.substring(start, end);
		str = str.replaceAll("\r\n", "");  // removes new lines
		str = str.replaceAll("\t", "");  // removes new lines
		str = str.replaceAll(" {2}", "");  // removes double spaces
		str = str.replaceAll("([b][o][u][n][d][s][A])(.+?)([)][;])", "");  // removes unnecessary text
		str = str.replaceAll("([m][a][p][p][i][n][g])(.+?)([1][;])", "");  // removes unnecessary text
		str = str.replaceAll("([w][i][d][g][e][t])(.+?)(?=[0-9])", "{coordinates:\""); // removes commas after last entries
		str = str.replaceAll("(?<=[0-9])([,][ ]['])", "\", city:'");
		str = str.replaceAll("((?<=['])([,][ ]['])(.+?)([e][)][;]))", "},");
		str = str.substring(0, str.length() - 5); // removes comma after last entry
		str = "{\"offices\":[" + str + "]}";
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, ProcreditObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		ProcreditObject.Offices[] bankAtms = ((ProcreditObject) bankObject).getOffices();
		for (ProcreditObject.Offices bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.PROCREDIT);
			atm.setCity(bankAtm.getCity());
			int index = bankAtm.getCoordinates().indexOf(',');
			String lat = bankAtm.getCoordinates().substring(0, index);
			String lon = bankAtm.getCoordinates().substring(index + 1, bankAtm.getCoordinates().length());
			atm.setLat(Double.parseDouble(lat));
			atm.setLon(Double.parseDouble(lon));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		ProcreditObject.Offices[] bankAtms = ((ProcreditObject) bankObject).getOffices();
		for (ProcreditObject.Offices bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.PROCREDIT);
			int index = bankAtm.getCoordinates().indexOf(',');
			String lat = bankAtm.getCoordinates().substring(0, index);
			String lon = bankAtm.getCoordinates().substring(index + 1, bankAtm.getCoordinates().length());
			atm.setLat(Double.parseDouble(lat));
			atm.setLon(Double.parseDouble(lon));
			atms.add(atm);
		}
		return atms;
	}
}
