package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.bank.CibankObject;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 16.8.2016 г..
 */
public class CibankParser extends ParserBase {
	private static final String URL = "";
	private static final String DIR = "json";
	private static final String BANK_NAME = "Cibank";
	private static final String FILE_NAME = "cibank.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		str = "{\"offices\":" + str + "}";
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, CibankObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		CibankObject.Offices[] bankAtms = ((CibankObject) bankObject).getOffices();
		for (CibankObject.Offices bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.CIBANK);
			atm.setLat(Double.parseDouble(bankAtm.getLat()));
			atm.setLon(Double.parseDouble(bankAtm.getLon()));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		CibankObject.Offices[] bankAtms = ((CibankObject) bankObject).getOffices();
		for (CibankObject.Offices bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.CIBANK);
			atm.setLat(Double.parseDouble(bankAtm.getLat()));
			atm.setLon(Double.parseDouble(bankAtm.getLon()));
			atms.add(atm);
		}
		return atms;
	}
}
