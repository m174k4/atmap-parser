package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.bank.PiraeusObject;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 17.8.2016.
 */
public class PiraeusParser extends ParserBase {
	private static final String URL = "https://www.piraeusbank.bg/piraeus-bank-bulgaria/branches-atms.html";
	private static final String DIR = "other";
	private static final String BANK_NAME = "Piraeus";
	private static final String FILE_NAME = "piraeus.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		str = str.replaceAll("\r\n", "");  // removes new lines
		int start = str.indexOf("$(document).ready(function () {MapInfoArray") + 31;
		int end = str.indexOf("$('#atm_city').ready(function () {", start);
		str = str.substring(start, end);
		str = str.replaceAll("([M][a][p][I][n][f][o])(.+?)(?=[M][a][p][A][T][M][s])", "");  // removes branches
		str = str.replaceAll("([M][a][p][A][T][M][s][A][r][r][a][y][\\[][0-9]+[\\]]) (.+?)([(][)][;])", "},{");  // removes new array
		str = str.replaceAll("([;][}][,][{])", "},{"); // removes commas after last entries
		str = str.replaceAll("([M][a][p][A][T][M][s][A][r][r][a][y][\\[][0-9]+[\\]][\\[][0])(.+?)(?=['])", "city:");
		str = str.replaceAll("([M][a][p][A][T][M][s][A][r][r][a][y][\\[][0-9]+[\\]][\\[][1])(.+?)(?=['])", "address:");
		str = str.replaceAll("([M][a][p][A][T][M][s][A][r][r][a][y][\\[][0-9]+[\\]][\\[][2])(.+?)(?=[0-9])", "lat:");
		str = str.replaceAll("([M][a][p][A][T][M][s][A][r][r][a][y][\\[][0-9]+[\\]][\\[][3])(.+?)(?=[0-9])", "lon:");
		str = str.replaceAll("([;])", ",");
		str = str.substring(2, str.length() - 9); // removes comma after last entry
		str = "{\"offices\":[" + str + "}]}";
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, PiraeusObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		PiraeusObject.Offices[] bankAtms = ((PiraeusObject) bankObject).getOffices();
		for (PiraeusObject.Offices bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.PIRAEUS);
			atm.setCity(bankAtm.getCity());
			atm.setLat(Double.parseDouble(bankAtm.getLat()));
			atm.setLon(Double.parseDouble(bankAtm.getLon()));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		PiraeusObject.Offices[] bankAtms = ((PiraeusObject) bankObject).getOffices();
		for (PiraeusObject.Offices bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.PIRAEUS);
			atm.setLat(Double.parseDouble(bankAtm.getLat()));
			atm.setLon(Double.parseDouble(bankAtm.getLon()));
			atms.add(atm);
		}
		return atms;
	}
}
