package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.bank.IabObject;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 16.8.2016 г..
 */
public class IabParser extends ParserBase {
	private static final String URL = "http://www.iabank.bg/contacts/offices";
	private static final String DIR = "json";
	private static final String BANK_NAME = "Iab";
	private static final String FILE_NAME = "iab.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		str = "{\"offices\":" + str + "}";
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, IabObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		IabObject.Offices[] bankAtms = ((IabObject) bankObject).getOffices();
		for (IabObject.Offices bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.IAB);
			atm.setAddress(bankAtm.getOffice_name());
			int index = bankAtm.getLatlng().indexOf(',');
			String lat = bankAtm.getLatlng().substring(0, index);
			String lon = bankAtm.getLatlng().substring(index + 1, bankAtm.getLatlng().length());
			atm.setLat(Double.parseDouble(lat));
			atm.setLon(Double.parseDouble(lon));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		IabObject.Offices[] bankAtms = ((IabObject) bankObject).getOffices();
		for (IabObject.Offices bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.IAB);
			int index = bankAtm.getLatlng().indexOf(',');
			String lat = bankAtm.getLatlng().substring(0, index);
			String lon = bankAtm.getLatlng().substring(index + 1, bankAtm.getLatlng().length());
			atm.setLat(Double.parseDouble(lat));
			atm.setLon(Double.parseDouble(lon));
			atms.add(atm);
		}
		return atms;
	}
}
