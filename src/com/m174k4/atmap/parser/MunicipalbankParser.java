package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.bank.MunicipalbankObject;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 17.8.2016.
 */
public class MunicipalbankParser extends ParserBase {
	private static final String URL = "http://www.municipalbank.bg/displaybg.aspx?page=individ_37";
	private static final String DIR = "other";
	private static final String BANK_NAME = "MunicipalBank";
	private static final String FILE_NAME = "municipalbank.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		int start = str.indexOf("var map_info = new Array();") + 27;
		int end = str.indexOf("</script>", start);
		str = str.substring(start, end);
		str = str.replaceAll("([m][a][p])(.+?)(?=[L][a])", "{");  // removes first part
		str = str.replaceAll("([p][h][o])(.+?)(['][,])", ""); //removes phone
		str = str.replaceAll("([}][;])", "},");
		str = str.replaceAll("(?<=[0-9]),(?=[0-9])", "."); //changes decimal comma to point
		str = str.substring(0, str.length() - 8); // removes comma after last entry
		str = "{\"offices\":[" + str + "]}";
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, MunicipalbankObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		MunicipalbankObject.Offices[] bankAtms = ((MunicipalbankObject) bankObject).getOffices();
		for (MunicipalbankObject.Offices bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.MUNICIPAL_BANK);
			atm.setCity(bankAtm.getCity());
			atm.setLat(Double.parseDouble(bankAtm.getLatLngX())); // TODO: Check if x and y are not reversed
			atm.setLon(Double.parseDouble(bankAtm.getLatLngY()));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		MunicipalbankObject.Offices[] bankAtms = ((MunicipalbankObject) bankObject).getOffices();
		for (MunicipalbankObject.Offices bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.MUNICIPAL_BANK);
			atm.setLat(Double.parseDouble(bankAtm.getLatLngX())); // TODO: Check if x and y are not reversed
			atm.setLon(Double.parseDouble(bankAtm.getLatLngY()));
			atms.add(atm);
		}
		return atms;
	}
}
