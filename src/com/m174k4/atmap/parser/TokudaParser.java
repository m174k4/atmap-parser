package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.bank.TokudaObject;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 16.8.2016 г..
 */
public class TokudaParser extends ParserBase {
	private static final String URL = "https://www.tokudabank.bg/en/offices";
	private static final String DIR = "json";
	private static final String BANK_NAME = "Tokuda";
	private static final String FILE_NAME = "tokuda.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		int start = str.indexOf("var atms = ") + 11;
		int end = str.indexOf("}];", start) + 2;
		str = str.substring(start, end);
		str = "{\"offices\":" + str + "}";
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, TokudaObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		TokudaObject.Offices[] bankAtms = ((TokudaObject) bankObject).getOffices();
		for (TokudaObject.Offices bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.TOKUDA);
			atm.setAddress(bankAtm.getTitle());
			atm.setLat(Double.parseDouble(bankAtm.getLatitude()));
			atm.setLon(Double.parseDouble(bankAtm.getLongtitude()));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		TokudaObject.Offices[] bankAtms = ((TokudaObject) bankObject).getOffices();
		for (TokudaObject.Offices bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.TOKUDA);
			atm.setLat(Double.parseDouble(bankAtm.getLatitude()));
			atm.setLon(Double.parseDouble(bankAtm.getLongtitude()));
			atms.add(atm);
		}
		return atms;
	}
}
