package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.bank.FIBObject;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 16.8.2016 г..
 */
public class FibParser extends ParserBase {
	private static final String URL = "https://www.fibank.bg/bg/server/show/operate/ATM-results/key/ALL/lt/0/addkey/";
	private static final String DIR = "json";
	private static final String BANK_NAME = "Fib";
	private static final String FILE_NAME = "fib.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {

		int start = str.indexOf("markersNew = ") + 13;
		int end = str.indexOf("}}\t\t]") + 3;
		str = str.substring(start, end);
		str = str.replaceAll("\r\n", "");  // removes new lines
		str = str.replaceAll("\t", "");  // removes new lines
		str = "{\"offices\":" + str + "]}";
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, FIBObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		FIBObject.Offices[] bankAtms = ((FIBObject) bankObject).getOffices();
		for (FIBObject.Offices bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.FIB);
			atm.setAddress(bankAtm.getAddress());
			atm.setLat(Double.parseDouble(bankAtm.getLat()));
			atm.setLon(Double.parseDouble(bankAtm.getLng()));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		FIBObject.Offices[] bankAtms = ((FIBObject) bankObject).getOffices();
		for (FIBObject.Offices bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.FIB);
			atm.setLat(Double.parseDouble(bankAtm.getLat()));
			atm.setLon(Double.parseDouble(bankAtm.getLng()));
			atms.add(atm);
		}
		return atms;
	}
}
