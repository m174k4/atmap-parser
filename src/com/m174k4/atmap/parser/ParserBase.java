package com.m174k4.atmap.parser;

import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.utils.FileUtils;
import com.m174k4.atmap.utils.UrlUtils;

import java.util.List;

/*
 * Created by Dimitar Ralev on 16.8.2016 г..
 */
public abstract class ParserBase implements Parser {
	protected abstract String getURL();

	protected abstract String getDir();

	protected abstract String getBankName();

	protected abstract String getBankFileName();

	public List<ATM> parse(boolean full) {
		System.out.println("Reading from URL for " + getBankName() + "..");
		String bankContent = "";

		if (getURL() != null && getURL().length() > 0) {
			if (getBankName().equals("IBank")) {
				bankContent = UrlUtils.getPageSource(getURL());
			}
			else {
				bankContent = UrlUtils.readURL(getURL());
			}

			boolean isValid = ensureParsing(bankContent);

			if (isValid) {
				//TODO Write to file
			}
		}

		if (bankContent == null || bankContent.length() == 0) {
			System.out.println("Error in URL, fallback to file for " + getBankName() + "..");
			bankContent = FileUtils.readFile(getDir(), getBankFileName());
		}

		boolean isValid = ensureParsing(bankContent);

		if (isValid) {
			System.out.println("Parsing " + getBankName() + "..");
			if (full) {
				return parseFull(read(bankContent));
			} else {
				return parseLight(read(bankContent));
			}
		} else {
			return null;
		}
	}

	private boolean ensureParsing (String bankContent) {
		try {
			Object bankObject = read(bankContent);
			parseFull(bankObject);
			parseLight(bankObject);
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	protected abstract <T> T read(String str);

	protected abstract <T> List<ATM> parseFull(T bankObject);

	protected abstract <T> List<ATM> parseLight(T bankObject);
}
