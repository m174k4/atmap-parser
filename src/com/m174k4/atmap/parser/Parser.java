package com.m174k4.atmap.parser;

import com.m174k4.atmap.model.ATM;

import java.util.List;

public interface Parser {
	List<ATM> parse(boolean full);
}
