package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;
import com.m174k4.atmap.model.bank.SgebObject;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 16.8.2016 г..
 */
public class SgebParser extends ParserBase {
	private static final String URL = "http://www.sgeb.bg/bg/byrzi-vryzki/klonove-i-bankomati.html";
	private static final String DIR = "json";
	private static final String BANK_NAME = "Sgeb";
	private static final String FILE_NAME = "sgeb.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		int start = str.indexOf("var offices = ") + 14;
		int end = str.indexOf("}};", start) + 2;
		str = str.substring(start, end);
		str = str.replaceAll("[\"][1][\"][:]", "\"atms\":[");
		str = str.replaceAll("[\"][0-9]+[\"][:]", "");
		str = str.substring(0, str.length() - 1);
		str = str + "]}";
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, SgebObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		SgebObject.Atms[] bankAtms = ((SgebObject) bankObject).getAtms();
		for (SgebObject.Atms bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.SGEB);
			atm.setAddress(bankAtm.getAddress());
			int index = bankAtm.getCoordinates().indexOf(',');
			String lat = bankAtm.getCoordinates().substring(0, index);
			String lon = bankAtm.getCoordinates().substring(index + 1, bankAtm.getCoordinates().length());
			atm.setLat(Double.parseDouble(lat));
			atm.setLon(Double.parseDouble(lon));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		SgebObject.Atms[] bankAtms = ((SgebObject) bankObject).getAtms();
		for (SgebObject.Atms bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.SGEB);
			int index = bankAtm.getCoordinates().indexOf(',');
			String lat = bankAtm.getCoordinates().substring(0, index);
			String lon = bankAtm.getCoordinates().substring(index + 1, bankAtm.getCoordinates().length());
			atm.setLat(Double.parseDouble(lat));
			atm.setLon(Double.parseDouble(lon));
			atms.add(atm);
		}
		return atms;
	}
}
