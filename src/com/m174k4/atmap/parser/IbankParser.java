package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;
import com.m174k4.atmap.model.bank.IbankObject;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 17.8.2016.
 */
public class IbankParser extends ParserBase {
	private static final String URL = "http://ibank.bg/%D1%84%D0%B8%D0%BD%D0%B0%D0%BD%D1%81%D0%BE%D0%B2%D0%B8-%D1%86%D0%B5%D0%BD%D1%82%D1%80%D0%BE%D0%B2%D0%B5-%D0%B8-atm/";
	private static final String DIR = "other";
	private static final String BANK_NAME = "IBank";
	private static final String FILE_NAME = "ibank.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		int start = str.indexOf("$(\".gmap\").gMap(") + 16;
		int end = str.indexOf("zoom: 13\n        });", start) + 18;
		str = str.substring(start, end);
		str = str.replaceAll("\r\n", "");  // removes new lines
		str = str.replaceAll("\n", "");  // removes new lines
		str = str.replaceAll("  ", "");  // removes double spaces
		str = str.replaceAll("	", "");  // removes tabs spaces
		str = str.replaceAll("([,][i])(.+?)([)][}])", "}");  // removes icon and html
		str = str.replaceAll("([,][]])", "]");  // removes lat comma
		str = str.replaceAll("([,][p][o][p][u][p])(.+?)([1][3])", ""); // removes last part
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, IbankObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		IbankObject.Markers[] bankAtms = ((IbankObject) bankObject).getMarkers();
		for (IbankObject.Markers bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.IBANK);
			atm.setLat(Double.parseDouble(bankAtm.getLatitude()));
			atm.setLon(Double.parseDouble(bankAtm.getLongitude()));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		IbankObject.Markers[] bankAtms = ((IbankObject) bankObject).getMarkers();
		for (IbankObject.Markers bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.IBANK);
			atm.setLat(Double.parseDouble(bankAtm.getLatitude()));
			atm.setLon(Double.parseDouble(bankAtm.getLongitude()));
			atms.add(atm);
		}
		return atms;
	}
}
