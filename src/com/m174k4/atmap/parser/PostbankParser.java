package com.m174k4.atmap.parser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.m174k4.atmap.model.bank.PostbankObject;
import com.m174k4.atmap.model.ATM;
import com.m174k4.atmap.model.ATMFull;
import com.m174k4.atmap.model.ATMLight;
import com.m174k4.atmap.model.Banks;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dimitar Ralev on 16.8.2016 г..
 */
public class PostbankParser extends ParserBase {
	private static final String URL = "https://www.postbank.bg/Applications/Locations/atm/bg/list/?d=1471011300195";
	private static final String DIR = "json";
	private static final String BANK_NAME = "Postbank";
	private static final String FILE_NAME = "postbank.txt";

	@Override
	protected String getURL() {
		return URL;
	}

	@Override
	protected String getDir() {
		return DIR;
	}

	@Override
	protected String getBankName() {
		return BANK_NAME;
	}

	@Override
	protected String getBankFileName() {
		return FILE_NAME;
	}

	protected <T> T read(String str) {
		str = "{\"offices\":" + str + "}";
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(str));
		reader.setLenient(true);
		return gson.fromJson(reader, PostbankObject.class);
	}

	protected <T> List<ATM> parseFull(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		PostbankObject.Offices[] bankAtms = ((PostbankObject) bankObject).getOffices();
		for (PostbankObject.Offices bankAtm : bankAtms) {
			ATMFull atm = new ATMFull();
			atm.setBank(Banks.POST_BANK);
			atm.setAddress(bankAtm.getAddress());
			atm.setLat(Double.parseDouble(bankAtm.getLat()));
			atm.setLon(Double.parseDouble(bankAtm.getLng()));
			atms.add(atm);
		}
		return atms;
	}

	protected <T> List<ATM> parseLight(T bankObject) {
		List<ATM> atms = new ArrayList<>();
		PostbankObject.Offices[] bankAtms = ((PostbankObject) bankObject).getOffices();
		for (PostbankObject.Offices bankAtm : bankAtms) {
			ATMLight atm = new ATMLight();
			atm.setBank(Banks.POST_BANK);
			atm.setLat(Double.parseDouble(bankAtm.getLat()));
			atm.setLon(Double.parseDouble(bankAtm.getLng()));
			atms.add(atm);
		}
		return atms;
	}
}
