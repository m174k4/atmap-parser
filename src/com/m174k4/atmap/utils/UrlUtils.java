package com.m174k4.atmap.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/*
 * Created by Dimitar Ralev on 16.8.2016.
 */
public class UrlUtils {
	public static String readURL(String url) {
		try {
			URLConnection connection = new URL(url).openConnection();
			Scanner scanner = new Scanner(connection.getInputStream());
			scanner.useDelimiter("\\Z");

			return scanner.next();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static String getPageSource(String url) {
		try {
			Document doc = Jsoup.connect(url).get();
			String source = doc.toString();
			source = source.replace("<html>\n" + " <head></head>\n" + " <body>", "");
			source = source.replace(" </body>\n" + "</html>", "");
			return source;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
