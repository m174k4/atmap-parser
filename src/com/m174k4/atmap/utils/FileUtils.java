package com.m174k4.atmap.utils;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

/*
 * Created by Dimitar Ralev on 16.8.2016.
 */
public class FileUtils {
    private static final String FILE_NAME_FORMATTED_ = "ATMsJSON_Formatted.json";
    private static final String FILE_NAME_ENC = "ATMsJSON.json";
    private static final String CIPHER = "AES/CBC/PKCS5Padding";
    private static final String ALGORITHM = "PBKDF2WithHmacSHA1";
    private static final String PASSWORD = "mrxsGs1nVvX0BxM4";

    public static String readFile(String dir, String fileName) {
        byte[] encoded = new byte[0];
        try {
            encoded = Files.readAllBytes(Paths.get("assets/" + dir + "/" + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(encoded, StandardCharsets.UTF_8);
    }

    public static void writeFile(String json) {
        try {
            Files.deleteIfExists(Paths.get(FILE_NAME_FORMATTED_));
            Files.write(Paths.get(FILE_NAME_FORMATTED_), json.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeEncryptedFile(String str) {
        try {
            Files.deleteIfExists(Paths.get(FILE_NAME_ENC));
            Files.write(Paths.get(FILE_NAME_ENC), encrypt(str), StandardOpenOption.CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static SecretKey generateKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        final byte[] salt = new byte[]{(byte) 0x3d, (byte) 0xb8, (byte) 0xda, (byte) 0xb7, (byte) 0x8e, (byte) 0x7a, (byte) 0x8d, (byte) 0xee, (byte) 0xf8, (byte) 0xca, (byte) 0xfc, (byte) 0xf7, (byte) 0x35, (byte) 0x0f, (byte) 0xda, (byte) 0x67};

        SecretKeyFactory factory = SecretKeyFactory.getInstance(ALGORITHM);
        KeySpec spec = new PBEKeySpec(PASSWORD.toCharArray(), salt, 65536, 128);
        SecretKey tmp = factory.generateSecret(spec);
        return new SecretKeySpec(tmp.getEncoded(), "AES");
    }

    private static IvParameterSpec generateIv() throws NoSuchAlgorithmException, InvalidKeySpecException {
        final byte[] iv = new byte[]{(byte) 0x1f, (byte) 0x36, (byte) 0xa8, (byte) 0x25, (byte) 0xbd, (byte) 0x5a, (byte) 0xc0, (byte) 0x3c, (byte) 0xbe, (byte) 0x5e, (byte) 0x83, (byte) 0xce, (byte) 0xd7, (byte) 0xd1, (byte) 0xfe, (byte) 0x2f};
        return new IvParameterSpec(iv);
    }

    private static byte[] encrypt(String message) throws Exception {
        Cipher cipher = Cipher.getInstance(CIPHER);
        cipher.init(Cipher.ENCRYPT_MODE, generateKey(), generateIv());
        return cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));
    }

    private byte[] decrypt(byte[] cipherText) throws Exception {
        Cipher cipher = Cipher.getInstance(CIPHER);
        cipher.init(Cipher.DECRYPT_MODE, generateKey(), generateIv());
        return cipher.doFinal(cipherText);
    }
}
