package com.m174k4.atmap.model;

/*
 * Created by Dimitar Ralev on 16.8.2016.
 */
public class ATMLight implements ATM {
    private Banks bank;
    private double lat;
    private double lon;

    public Banks getBank() {
        return bank;
    }

    public void setBank(Banks bank) {
        this.bank = bank;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
