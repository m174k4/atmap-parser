package com.m174k4.atmap.model;

/*
 * Created by Dimitar Ralev on 16.8.2016.
 */
public enum Banks {
    DSK,
    UBB,
    FIB,
    RAIFAIZEN,
    UNICREDIT,
    SGEB,
    ALLIANZ,
    PIRAEUS,
    IBANK,
    IAB,
    MUNICIPAL_BANK,
    POST_BANK,
    PROCREDIT,
    CIBANK,
    TOKUDA,
    DBANK,
    CCB
}
