package com.m174k4.atmap.model;

public interface ATM {
	Banks getBank();

	double getLat();

	double getLon();
}
