package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 17.8.2016.
 */
public class FIBObject {
	private Offices[] offices;

	public Offices[] getOffices() {
		return offices;
	}

	public void setOffices(Offices[] offices) {
		this.offices = offices;
	}

	@Override
	public String toString() {
		return "ClassPojo [offices = " + offices + "]";
	}

	public class Offices {
		private String title;

		private String address;

		private String lng;

		private String lat;

		private Info info;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getLng() {
			return lng;
		}

		public void setLng(String lng) {
			this.lng = lng;
		}

		public String getLat() {
			return lat;
		}

		public void setLat(String lat) {
			this.lat = lat;
		}

		public Info getInfo() {
			return info;
		}

		public void setInfo(Info info) {
			this.info = info;
		}

		@Override
		public String toString() {
			return "ClassPojo [title = " + title + ", address = " + address + ", lng = " + lng + ", lat = " + lat + ", info = " + info + "]";
		}
	}

	public class Info {
		private String title;

		private String address;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		@Override
		public String toString() {
			return "ClassPojo [title = " + title + ", address = " + address + "]";
		}
	}
}
