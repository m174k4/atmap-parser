package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 18.8.2016.
 */
public class ProcreditObject {
    private Offices[] offices;

    public Offices[] getOffices() {
        return offices;
    }

    public void setOffices(Offices[] offices) {
        this.offices = offices;
    }

    @Override
    public String toString() {
        return "ClassPojo [offices = " + offices + "]";
    }

    public class Offices {
        private String coordinates;

        private String city;

        public String getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(String coordinates) {
            this.coordinates = coordinates;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        @Override
        public String toString() {
            return "ClassPojo [coordinates = " + coordinates + ", city = " + city + "]";
        }
    }
}
