package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 17.8.2016.
 */
public class IbankObject {
    private String address;

    private Markers[] markers;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Markers[] getMarkers() {
        return markers;
    }

    public void setMarkers(Markers[] markers) {
        this.markers = markers;
    }

    @Override
    public String toString() {
        return "ClassPojo [address = " + address + ", markers = " + markers + "]";
    }

    public class Markers {
        private String longitude;

        private String latitude;

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        @Override
        public String toString() {
            return "ClassPojo [longitude = " + longitude + ", latitude = " + latitude + "]";
        }
    }
}
