package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 17.8.2016 г..
 */
public class PostbankObject {
    private Offices[] offices;

    public Offices[] getOffices() {
        return offices;
    }

    public void setOffices(Offices[] offices) {
        this.offices = offices;
    }

    @Override
    public String toString() {
        return "ClassPojo [offices = " + offices + "]";
    }


    public class Offices {
        private String id;

        private String Name;

        private String zip;

        private String phone;

        private String address;

        private WorkTime workTime;

        private String lng;

        private String type;

        private String lat;

        private String fc;

        private String country;

        private String city;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public WorkTime getWorkTime() {
            return workTime;
        }

        public void setWorkTime(WorkTime workTime) {
            this.workTime = workTime;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getFc() {
            return fc;
        }

        public void setFc(String fc) {
            this.fc = fc;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        @Override
        public String toString() {
            return "ClassPojo [id = " + id + ", Name = " + Name + ", zip = " + zip + ", phone = " + phone + ", address = " + address + ", workTime = " + workTime + ", lng = " + lng + ", type = " + type + ", lat = " + lat + ", fc = " + fc + ", country = " + country + ", city = " + city + "]";
        }
    }


    public class WorkTime {
        private WorkDays workDays;

        private WorkSaturday workSaturday;

        private WorkSunday workSunday;

        public WorkDays getWorkDays() {
            return workDays;
        }

        public void setWorkDays(WorkDays workDays) {
            this.workDays = workDays;
        }

        public WorkSaturday getWorkSaturday() {
            return workSaturday;
        }

        public void setWorkSaturday(WorkSaturday workSaturday) {
            this.workSaturday = workSaturday;
        }

        public WorkSunday getWorkSunday() {
            return workSunday;
        }

        public void setWorkSunday(WorkSunday workSunday) {
            this.workSunday = workSunday;
        }

        @Override
        public String toString() {
            return "ClassPojo [workDays = " + workDays + ", workSaturday = " + workSaturday + ", workSunday = " + workSunday + "]";
        }
    }

    public class WorkSunday {
        private String to;

        private String isWorking;

        private String from;

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getIsWorking() {
            return isWorking;
        }

        public void setIsWorking(String isWorking) {
            this.isWorking = isWorking;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        @Override
        public String toString() {
            return "ClassPojo [to = " + to + ", isWorking = " + isWorking + ", from = " + from + "]";
        }
    }

    public class WorkSaturday {
        private String to;

        private String isWorking;

        private String from;

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getIsWorking() {
            return isWorking;
        }

        public void setIsWorking(String isWorking) {
            this.isWorking = isWorking;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        @Override
        public String toString() {
            return "ClassPojo [to = " + to + ", isWorking = " + isWorking + ", from = " + from + "]";
        }
    }

    public class WorkDays {
        private String to;

        private String isWorking;

        private String from;

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getIsWorking() {
            return isWorking;
        }

        public void setIsWorking(String isWorking) {
            this.isWorking = isWorking;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        @Override
        public String toString() {
            return "ClassPojo [to = " + to + ", isWorking = " + isWorking + ", from = " + from + "]";
        }
    }
}
