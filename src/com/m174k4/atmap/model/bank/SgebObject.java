package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 17.8.2016.
 */
public class SgebObject {
    private Atms[] atms;

    public Atms[] getAtms() {
        return atms;
    }

    public void setAtms(Atms[] atms) {
        this.atms = atms;
    }

    @Override
    public String toString() {
        return "ClassPojo [atms = " + atms + "]";
    }

    public class Atms {
        private String office;

        private String phone;

        private String fax;

        private String working_saturday;

        private String branch_type;

        private String wu;

        private String city;

        private String atm;

        private String fax_label;

        private String workhours;

        private String atm_work_label;

        private String address;

        private String atm_label;

        private String workhours_label;

        private String group;

        private String city_title;

        private String extended_hours;

        private String coordinates;

        private String phone_label;

        public String getOffice() {
            return office;
        }

        public void setOffice(String office) {
            this.office = office;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getFax() {
            return fax;
        }

        public void setFax(String fax) {
            this.fax = fax;
        }

        public String getWorking_saturday() {
            return working_saturday;
        }

        public void setWorking_saturday(String working_saturday) {
            this.working_saturday = working_saturday;
        }

        public String getBranch_type() {
            return branch_type;
        }

        public void setBranch_type(String branch_type) {
            this.branch_type = branch_type;
        }

        public String getWu() {
            return wu;
        }

        public void setWu(String wu) {
            this.wu = wu;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAtm() {
            return atm;
        }

        public void setAtm(String atm) {
            this.atm = atm;
        }

        public String getFax_label() {
            return fax_label;
        }

        public void setFax_label(String fax_label) {
            this.fax_label = fax_label;
        }

        public String getWorkhours() {
            return workhours;
        }

        public void setWorkhours(String workhours) {
            this.workhours = workhours;
        }

        public String getAtm_work_label() {
            return atm_work_label;
        }

        public void setAtm_work_label(String atm_work_label) {
            this.atm_work_label = atm_work_label;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAtm_label() {
            return atm_label;
        }

        public void setAtm_label(String atm_label) {
            this.atm_label = atm_label;
        }

        public String getWorkhours_label() {
            return workhours_label;
        }

        public void setWorkhours_label(String workhours_label) {
            this.workhours_label = workhours_label;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public String getCity_title() {
            return city_title;
        }

        public void setCity_title(String city_title) {
            this.city_title = city_title;
        }

        public String getExtended_hours() {
            return extended_hours;
        }

        public void setExtended_hours(String extended_hours) {
            this.extended_hours = extended_hours;
        }

        public String getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(String coordinates) {
            this.coordinates = coordinates;
        }

        public String getPhone_label() {
            return phone_label;
        }

        public void setPhone_label(String phone_label) {
            this.phone_label = phone_label;
        }

        @Override
        public String toString() {
            return "ClassPojo [office = " + office + ", phone = " + phone + ", fax = " + fax + ", working_saturday = " + working_saturday + ", branch_type = " + branch_type + ", wu = " + wu + ", city = " + city + ", atm = " + atm + ", fax_label = " + fax_label + ", workhours = " + workhours + ", atm_work_label = " + atm_work_label + ", address = " + address + ", atm_label = " + atm_label + ", workhours_label = " + workhours_label + ", group = " + group + ", city_title = " + city_title + ", extended_hours = " + extended_hours + ", coordinates = " + coordinates + ", phone_label = " + phone_label + "]";
        }
    }
}
