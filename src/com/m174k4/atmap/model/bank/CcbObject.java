package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 17.8.2016.
 */
public class CcbObject {
	private Offices[] offices;

	public Offices[] getOffices() {
		return offices;
	}

	public void setOffices(Offices[] offices) {
		this.offices = offices;
	}

	@Override
	public String toString() {
		return "ClassPojo [offices = " + offices + "]";
	}

	public class Offices {
		private String id;

		private String phone;

		private String marker;

		private String address;

		private String store_id;

		private String longitude;

		private String latitude;

		private String city;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getMarker() {
			return marker;
		}

		public void setMarker(String marker) {
			this.marker = marker;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getStore_id() {
			return store_id;
		}

		public void setStore_id(String store_id) {
			this.store_id = store_id;
		}

		public String getLongitude() {
			return longitude;
		}

		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}

		public String getLatitude() {
			return latitude;
		}

		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		@Override
		public String toString() {
			return "ClassPojo [id = " + id + ", phone = " + phone + ", marker = " + marker + ", address = " + address + ", store_id = " + store_id + ", longitude = " + longitude + ", latitude = " + latitude + ", city = " + city + "]";
		}
	}
}
