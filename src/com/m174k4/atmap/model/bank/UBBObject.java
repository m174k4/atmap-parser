package com.m174k4.atmap.model.bank;

/*
 * Created by ralev on 13.8.2016.
 */
public class UBBObject {
	private String total;

	private Markers markers;

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public Markers getMarkers() {
		return markers;
	}

	public void setMarkers(Markers markers) {
		this.markers = markers;
	}

	@Override
	public String toString() {
		return "ClassPojo [total = " + total + ", markers = " + markers + "]";
	}

	public class Markers {
		private Offices[] offices;

		private Atms[] atms;

		public Offices[] getOffices() {
			return offices;
		}

		public void setOffices(Offices[] offices) {
			this.offices = offices;
		}

		public Atms[] getAtms() {
			return atms;
		}

		public void setAtms(Atms[] atms) {
			this.atms = atms;
		}

		@Override
		public String toString() {
			return "ClassPojo [offices = " + offices + ", atms = " + atms + "]";
		}
	}

	public class Atms {
		private Data data;

		private String[] latlng;

		public Data getData() {
			return data;
		}

		public void setData(Data data) {
			this.data = data;
		}

		public String[] getLatlng() {
			return latlng;
		}

		public void setLatlng(String[] latlng) {
			this.latlng = latlng;
		}

		@Override
		public String toString() {
			return "ClassPojo [data = " + data + ", latlng = " + latlng + "]";
		}
	}

	public class Offices {
		private Data data;

		private String[] latlng;

		public Data getData() {
			return data;
		}

		public void setData(Data data) {
			this.data = data;
		}

		public String[] getLatlng() {
			return latlng;
		}

		public void setLatlng(String[] latlng) {
			this.latlng = latlng;
		}

		@Override
		public String toString() {
			return "ClassPojo [data = " + data + ", latlng = " + latlng + "]";
		}
	}

	public class Data {
		private String region;

		private String phone;

		private String image;

		private String toponym;

		private String has_accessibility;

		private String lng;

		private String type;

		private String city;

		private String id;

		private String title;

		private String email;

		private String address;

		private String description;

//		private Features features;

		private String worktime;

		private String lat;

		private String bunch_note_acceptor;

		private String accessibility;

		public String getRegion() {
			return region;
		}

		public void setRegion(String region) {
			this.region = region;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

		public String getToponym() {
			return toponym;
		}

		public void setToponym(String toponym) {
			this.toponym = toponym;
		}

		public String getHas_accessibility() {
			return has_accessibility;
		}

		public void setHas_accessibility(String has_accessibility) {
			this.has_accessibility = has_accessibility;
		}

		public String getLng() {
			return lng;
		}

		public void setLng(String lng) {
			this.lng = lng;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

//		public Features getFeatures() {
//			return features;
//		}
//
//		public void setFeatures(Features features) {
//			this.features = features;
//		}

		public String getWorktime() {
			return worktime;
		}

		public void setWorktime(String worktime) {
			this.worktime = worktime;
		}

		public String getLat() {
			return lat;
		}

		public void setLat(String lat) {
			this.lat = lat;
		}

		public String getBunch_note_acceptor() {
			return bunch_note_acceptor;
		}

		public void setBunch_note_acceptor(String bunch_note_acceptor) {
			this.bunch_note_acceptor = bunch_note_acceptor;
		}

		public String getAccessibility() {
			return accessibility;
		}

		public void setAccessibility(String accessibility) {
			this.accessibility = accessibility;
		}

//		@Override
//		public String toString() {
//			return "ClassPojo [region = " + region + ", phone = " + phone + ", image = " + image + ", toponym = " + toponym + ", has_accessibility = " + has_accessibility + ", lng = " + lng + ", type = " + type + ", city = " + city + ", id = " + id + ", title = " + title + ", email = " + email + ", address = " + address + ", description = " + description + ", features = " + features + ", worktime = " + worktime + ", lat = " + lat + ", bunch_note_acceptor = " + bunch_note_acceptor + ", accessibility = " + accessibility + "]";
//		}
	}

//	public class Features {
//		private String 1;
//
//		private String 6;
//
//		private String 9;
//
//		private String 8;
//
//		private String 11;
//
//		public String get1() {
//			return 1;
//		}
//
//		public void set1(String 1) {
//			this .1 = 1;
//		}
//
//		public String get6() {
//			return 6;
//		}
//
//		public void set6(String 6) {
//			this .6 = 6;
//		}
//
//		public String get9() {
//			return 9;
//		}
//
//		public void set9(String 9) {
//			this .9 = 9;
//		}
//
//		public String get8() {
//			return 8;
//		}
//
//		public void set8(String 8) {
//			this .8 = 8;
//		}
//
//		public String get11() {
//			return 11;
//		}
//
//		public void set11(String 11) {
//			this .11 = 11;
//		}
//
//		@Override
//		public String toString() {
//			return "ClassPojo [1 = " + 1 + ", 6 = " + 6 + ", 9 = " + 9 + ", 8 = " + 8 + ", 11 = " + 11 + "]";
//		}
//	}
}
