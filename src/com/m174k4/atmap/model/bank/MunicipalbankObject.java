package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 18.8.2016 г..
 */
public class MunicipalbankObject {
    private Offices[] offices;

    public Offices[] getOffices() {
        return offices;
    }

    public void setOffices(Offices[] offices) {
        this.offices = offices;
    }

    @Override
    public String toString() {
        return "ClassPojo [offices = " + offices + "]";
    }


    public class Offices {
        private String addr;

        private String LatLngX;

        private String LatLngY;

        private String city;

        public String getAddr() {
            return addr;
        }

        public void setAddr(String addr) {
            this.addr = addr;
        }

        public String getLatLngX() {
            return LatLngX;
        }

        public void setLatLngX(String LatLngX) {
            this.LatLngX = LatLngX;
        }

        public String getLatLngY() {
            return LatLngY;
        }

        public void setLatLngY(String LatLngY) {
            this.LatLngY = LatLngY;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        @Override
        public String toString() {
            return "ClassPojo [addr = " + addr + ", LatLngX = " + LatLngX + ", LatLngY = " + LatLngY + ", city = " + city + "]";
        }
    }
}
