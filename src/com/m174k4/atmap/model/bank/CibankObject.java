package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 17.8.2016.
 */
public class CibankObject {
    private Offices[] offices;

    public Offices[] getOffices() {
        return offices;
    }

    public void setOffices(Offices[] offices) {
        this.offices = offices;
    }

    @Override
    public String toString() {
        return "ClassPojo [offices = " + offices + "]";
    }

    public class Offices {
        private String lon;

        private String lat;

        private String info;

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        @Override
        public String toString() {
            return "ClassPojo [lon = " + lon + ", lat = " + lat + ", info = " + info + "]";
        }
    }
}
