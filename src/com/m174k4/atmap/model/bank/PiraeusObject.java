package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 18.8.2016.
 */
public class PiraeusObject {
    private Offices[] offices;

    public Offices[] getOffices() {
        return offices;
    }

    public void setOffices(Offices[] offices) {
        this.offices = offices;
    }

    @Override
    public String toString() {
        return "ClassPojo [offices = " + offices + "]";
    }

    public class Offices {
        private String lon;

        private String address;

        private String lat;

        private String city;

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        @Override
        public String toString() {
            return "ClassPojo [lon = " + lon + ", address = " + address + ", lat = " + lat + ", city = " + city + "]";
        }
    }
}
