package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 17.8.2016 г..
 */
public class IabObject {
    private Offices[] offices;

    public Offices[] getOffices() {
        return offices;
    }

    public void setOffices(Offices[] offices) {
        this.offices = offices;
    }

    @Override
    public String toString() {
        return "ClassPojo [offices = " + offices + "]";
    }

    public class Offices {
        private String office_name;

        private String city_id;

        private String office_text_col_2;

        private String office_text_col_3;

        private String order;

        private String office_text_col_1;

        private String office_type;

        private String latlng;

        private String infowindow;

        private String branch_id;

        private String office_id;

        private String main_branch;

        public String getOffice_name() {
            return office_name;
        }

        public void setOffice_name(String office_name) {
            this.office_name = office_name;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getOffice_text_col_2() {
            return office_text_col_2;
        }

        public void setOffice_text_col_2(String office_text_col_2) {
            this.office_text_col_2 = office_text_col_2;
        }

        public String getOffice_text_col_3() {
            return office_text_col_3;
        }

        public void setOffice_text_col_3(String office_text_col_3) {
            this.office_text_col_3 = office_text_col_3;
        }

        public String getOrder() {
            return order;
        }

        public void setOrder(String order) {
            this.order = order;
        }

        public String getOffice_text_col_1() {
            return office_text_col_1;
        }

        public void setOffice_text_col_1(String office_text_col_1) {
            this.office_text_col_1 = office_text_col_1;
        }

        public String getOffice_type() {
            return office_type;
        }

        public void setOffice_type(String office_type) {
            this.office_type = office_type;
        }

        public String getLatlng() {
            return latlng;
        }

        public void setLatlng(String latlng) {
            this.latlng = latlng;
        }

        public String getInfowindow() {
            return infowindow;
        }

        public void setInfowindow(String infowindow) {
            this.infowindow = infowindow;
        }

        public String getBranch_id() {
            return branch_id;
        }

        public void setBranch_id(String branch_id) {
            this.branch_id = branch_id;
        }

        public String getOffice_id() {
            return office_id;
        }

        public void setOffice_id(String office_id) {
            this.office_id = office_id;
        }

        public String getMain_branch() {
            return main_branch;
        }

        public void setMain_branch(String main_branch) {
            this.main_branch = main_branch;
        }

        @Override
        public String toString() {
            return "ClassPojo [office_name = " + office_name + ", city_id = " + city_id + ", office_text_col_2 = " + office_text_col_2 + ", office_text_col_3 = " + office_text_col_3 + ", order = " + order + ", office_text_col_1 = " + office_text_col_1 + ", office_type = " + office_type + ", latlng = " + latlng + ", infowindow = " + infowindow + ", branch_id = " + branch_id + ", office_id = " + office_id + ", main_branch = " + main_branch + "]";
        }
    }
}
