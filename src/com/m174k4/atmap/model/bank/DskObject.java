package com.m174k4.atmap.model.bank;

/*
 * Created by ralev on 17.8.2016.
 */
public class DskObject {
	private Offices[] offices;

	public Offices[] getOffices() {
		return offices;
	}

	public void setOffices(Offices[] offices) {
		this.offices = offices;
	}

	@Override
	public String toString() {
		return "ClassPojo [offices = " + offices + "]";
	}


	public class Offices {
		private String Types;

		private String CityId;

		private String Location;

		private String Signature;

		private String City;

		private String Name;

		private String Text;

		private String Email;

		private String[] Services;

		private String OpenHours;

		private String Address;

		private String Latitude;

		private String Id;

		private String ServiceId;

		private String Longitude;

		private String RegionSfId;

		private String CitySfId;

		private String[] BranchType;

		public String getTypes() {
			return Types;
		}

		public void setTypes(String Types) {
			this.Types = Types;
		}

		public String getCityId() {
			return CityId;
		}

		public void setCityId(String CityId) {
			this.CityId = CityId;
		}

		public String getLocation() {
			return Location;
		}

		public void setLocation(String Location) {
			this.Location = Location;
		}

		public String getSignature() {
			return Signature;
		}

		public void setSignature(String Signature) {
			this.Signature = Signature;
		}

		public String getCity() {
			return City;
		}

		public void setCity(String City) {
			this.City = City;
		}

		public String getName() {
			return Name;
		}

		public void setName(String Name) {
			this.Name = Name;
		}

		public String getText() {
			return Text;
		}

		public void setText(String Text) {
			this.Text = Text;
		}

		public String getEmail() {
			return Email;
		}

		public void setEmail(String Email) {
			this.Email = Email;
		}

		public String[] getServices() {
			return Services;
		}

		public void setServices(String[] Services) {
			this.Services = Services;
		}

		public String getOpenHours() {
			return OpenHours;
		}

		public void setOpenHours(String OpenHours) {
			this.OpenHours = OpenHours;
		}

		public String getAddress() {
			return Address;
		}

		public void setAddress(String Address) {
			this.Address = Address;
		}

		public String getLatitude() {
			return Latitude;
		}

		public void setLatitude(String Latitude) {
			this.Latitude = Latitude;
		}

		public String getId() {
			return Id;
		}

		public void setId(String Id) {
			this.Id = Id;
		}

		public String getServiceId() {
			return ServiceId;
		}

		public void setServiceId(String ServiceId) {
			this.ServiceId = ServiceId;
		}

		public String getLongitude() {
			return Longitude;
		}

		public void setLongitude(String Longitude) {
			this.Longitude = Longitude;
		}

		public String getRegionSfId() {
			return RegionSfId;
		}

		public void setRegionSfId(String RegionSfId) {
			this.RegionSfId = RegionSfId;
		}

		public String getCitySfId() {
			return CitySfId;
		}

		public void setCitySfId(String CitySfId) {
			this.CitySfId = CitySfId;
		}

		public String[] getBranchType() {
			return BranchType;
		}

		public void setBranchType(String[] BranchType) {
			this.BranchType = BranchType;
		}

		@Override
		public String toString() {
			return "ClassPojo [Types = " + Types + ", CityId = " + CityId + ", Location = " + Location + ", Signature = " + Signature + ", City = " + City + ", Name = " + Name + ", Text = " + Text + ", Email = " + Email + ", Services = " + Services + ", OpenHours = " + OpenHours + ", Address = " + Address + ", Latitude = " + Latitude + ", Id = " + Id + ", ServiceId = " + ServiceId + ", Longitude = " + Longitude + ", RegionSfId = " + RegionSfId + ", CitySfId = " + CitySfId + ", BranchType = " + BranchType + "]";
		}
	}
}
