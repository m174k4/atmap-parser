package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 17.8.2016 г..
 */
public class TokudaObject {
    private Offices[] offices;

    public Offices[] getOffices() {
        return offices;
    }

    public void setOffices(Offices[] offices) {
        this.offices = offices;
    }

    @Override
    public String toString() {
        return "ClassPojo [offices = " + offices + "]";
    }

    public class Offices {
        private String Description;

        private String Culture;

        private String Availability;

        private String Latitude;

        private String Longtitude;

        private String Id;

        private String Title;

        private String TownId;

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getCulture() {
            return Culture;
        }

        public void setCulture(String Culture) {
            this.Culture = Culture;
        }

        public String getAvailability() {
            return Availability;
        }

        public void setAvailability(String Availability) {
            this.Availability = Availability;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getLongtitude() {
            return Longtitude;
        }

        public void setLongtitude(String Longtitude) {
            this.Longtitude = Longtitude;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getTownId() {
            return TownId;
        }

        public void setTownId(String TownId) {
            this.TownId = TownId;
        }

        @Override
        public String toString() {
            return "ClassPojo [Description = " + Description + ", Culture = " + Culture + ", Availability = " + Availability + ", Latitude = " + Latitude + ", Longtitude = " + Longtitude + ", Id = " + Id + ", Title = " + Title + ", TownId = " + TownId + "]";
        }
    }
}
