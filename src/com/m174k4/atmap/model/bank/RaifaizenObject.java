package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 17.8.2016.
 */
public class RaifaizenObject {
	private Atms[] atms;

	private Branches[] branches;

	public Atms[] getAtms() {
		return atms;
	}

	public void setAtms(Atms[] atms) {
		this.atms = atms;
	}

	public Branches[] getBranches() {
		return branches;
	}

	public void setBranches(Branches[] branches) {
		this.branches = branches;
	}

	@Override
	public String toString() {
		return "ClassPojo [atms = " + atms + ", branches = " + branches + "]";
	}

	public class Atms
	{
		private String index;

		private Location location;

		private String address;

		private String problem_description;

		private String description;

		private String name;

		private String operational;

		private String city;

		private String[] options;

		public String getIndex ()
		{
			return index;
		}

		public void setIndex (String index)
		{
			this.index = index;
		}

		public Location getLocation ()
		{
			return location;
		}

		public void setLocation (Location location)
		{
			this.location = location;
		}

		public String getAddress ()
		{
			return address;
		}

		public void setAddress (String address)
		{
			this.address = address;
		}

		public String getProblem_description ()
		{
			return problem_description;
		}

		public void setProblem_description (String problem_description)
		{
			this.problem_description = problem_description;
		}

		public String getDescription ()
		{
			return description;
		}

		public void setDescription (String description)
		{
			this.description = description;
		}

		public String getName ()
		{
			return name;
		}

		public void setName (String name)
		{
			this.name = name;
		}

		public String getOperational ()
		{
			return operational;
		}

		public void setOperational (String operational)
		{
			this.operational = operational;
		}

		public String getCity ()
		{
			return city;
		}

		public void setCity (String city)
		{
			this.city = city;
		}

		public String[] getOptions ()
		{
			return options;
		}

		public void setOptions (String[] options)
		{
			this.options = options;
		}

		@Override
		public String toString()
		{
			return "ClassPojo [index = "+index+", location = "+location+", address = "+address+", problem_description = "+problem_description+", description = "+description+", name = "+name+", operational = "+operational+", city = "+city+", options = "+options+"]";
		}
	}

	public class Branches
	{
		private Working_hours[] working_hours;

		private String phone;

		private String index;

		private String fax;

		private String post_code;

		private Location location;

		private String operational;

		private String city;

		private String problem_description;

		private String address;

		private String description;

		private String name;

		private String[] options;

		public Working_hours[] getWorking_hours ()
		{
			return working_hours;
		}

		public void setWorking_hours (Working_hours[] working_hours)
		{
			this.working_hours = working_hours;
		}

		public String getPhone ()
		{
			return phone;
		}

		public void setPhone (String phone)
		{
			this.phone = phone;
		}

		public String getIndex ()
		{
			return index;
		}

		public void setIndex (String index)
		{
			this.index = index;
		}

		public String getFax ()
		{
			return fax;
		}

		public void setFax (String fax)
		{
			this.fax = fax;
		}

		public String getPost_code ()
		{
			return post_code;
		}

		public void setPost_code (String post_code)
		{
			this.post_code = post_code;
		}

		public Location getLocation ()
		{
			return location;
		}

		public void setLocation (Location location)
		{
			this.location = location;
		}

		public String getOperational ()
		{
			return operational;
		}

		public void setOperational (String operational)
		{
			this.operational = operational;
		}

		public String getCity ()
		{
			return city;
		}

		public void setCity (String city)
		{
			this.city = city;
		}

		public String getProblem_description ()
		{
			return problem_description;
		}

		public void setProblem_description (String problem_description)
		{
			this.problem_description = problem_description;
		}

		public String getAddress ()
		{
			return address;
		}

		public void setAddress (String address)
		{
			this.address = address;
		}

		public String getDescription ()
		{
			return description;
		}

		public void setDescription (String description)
		{
			this.description = description;
		}

		public String getName ()
		{
			return name;
		}

		public void setName (String name)
		{
			this.name = name;
		}

		public String[] getOptions ()
		{
			return options;
		}

		public void setOptions (String[] options)
		{
			this.options = options;
		}

		@Override
		public String toString()
		{
			return "ClassPojo [working_hours = "+working_hours+", phone = "+phone+", index = "+index+", fax = "+fax+", post_code = "+post_code+", location = "+location+", operational = "+operational+", city = "+city+", problem_description = "+problem_description+", address = "+address+", description = "+description+", name = "+name+", options = "+options+"]";
		}
	}

	public class Location
	{
		private String lng;

		private String lat;

		public String getLng ()
		{
			return lng;
		}

		public void setLng (String lng)
		{
			this.lng = lng;
		}

		public String getLat ()
		{
			return lat;
		}

		public void setLat (String lat)
		{
			this.lat = lat;
		}

		@Override
		public String toString()
		{
			return "ClassPojo [lng = "+lng+", lat = "+lat+"]";
		}
	}

	public class Working_hours
	{
		private String model;

//		private Fields fields;

		private String pk;

		public String getModel ()
		{
			return model;
		}

		public void setModel (String model)
		{
			this.model = model;
		}

//		public Fields getFields ()
//		{
//			return fields;
//		}
//
//		public void setFields (Fields fields)
//		{
//			this.fields = fields;
//		}

		public String getPk ()
		{
			return pk;
		}

		public void setPk (String pk)
		{
			this.pk = pk;
		}

		@Override
		public String toString()
		{
			return "ClassPojo [model = "+model+", pk = "+pk+"]";
		}
	}

//	public class Fields
//	{
//		private null break_begin;
//
//		private String week_day;
//
//		private String branch;
//
//		private String end;
//
//		private null break_end;
//
//		private String begin;
//
//		public null getBreak_begin ()
//	{
//		return break_begin;
//	}
//
//		public void setBreak_begin (null break_begin)
//		{
//			this.break_begin = break_begin;
//		}
//
//		public String getWeek_day ()
//		{
//			return week_day;
//		}
//
//		public void setWeek_day (String week_day)
//		{
//			this.week_day = week_day;
//		}
//
//		public String getBranch ()
//		{
//			return branch;
//		}
//
//		public void setBranch (String branch)
//		{
//			this.branch = branch;
//		}
//
//		public String getEnd ()
//		{
//			return end;
//		}
//
//		public void setEnd (String end)
//		{
//			this.end = end;
//		}
//
//		public null getBreak_end ()
//	{
//		return break_end;
//	}
//
//		public void setBreak_end (null break_end)
//		{
//			this.break_end = break_end;
//		}
//
//		public String getBegin ()
//		{
//			return begin;
//		}
//
//		public void setBegin (String begin)
//		{
//			this.begin = begin;
//		}
//
//		@Override
//		public String toString()
//		{
//			return "ClassPojo [break_begin = "+break_begin+", week_day = "+week_day+", branch = "+branch+", end = "+end+", break_end = "+break_end+", begin = "+begin+"]";
//		}
//	}
}
