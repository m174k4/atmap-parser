package com.m174k4.atmap.model.bank;

/*
 * Created by ralev on 13.8.2016.
 */
public class UnicreditObject {
	private Data[] data;

	public Data[] getData() {
		return data;
	}

	public void setData(Data[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ClassPojo [data = " + data + "]";
	}

	public class Data {
		private String id;

		private String address;

		private String name;

		private String lng;

		private String lat;

		private String city;

		private Options[] options;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getLng() {
			return lng;
		}

		public void setLng(String lng) {
			this.lng = lng;
		}

		public String getLat() {
			return lat;
		}

		public void setLat(String lat) {
			this.lat = lat;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public Options[] getOptions() {
			return options;
		}

		public void setOptions(Options[] options) {
			this.options = options;
		}

		@Override
		public String toString() {
			return "ClassPojo [id = " + id + ", address = " + address + ", name = " + name + ", lng = " + lng + ", lat = " + lat + ", city = " + city + ", options = " + options + "]";
		}
	}

	public class Options {
		private String id;

		private String icon;

		private String name;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getIcon() {
			return icon;
		}

		public void setIcon(String icon) {
			this.icon = icon;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return "ClassPojo [id = " + id + ", icon = " + icon + ", name = " + name + "]";
		}
	}
}
