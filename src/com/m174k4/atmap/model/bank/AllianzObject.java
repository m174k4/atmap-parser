package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 17.8.2016 г..
 */
public class AllianzObject {
    private Offices[] offices;

    public Offices[] getOffices() {
        return offices;
    }

    public void setOffices(Offices[] offices) {
        this.offices = offices;
    }

    @Override
    public String toString() {
        return "ClassPojo [offices = " + offices + "]";
    }

    public class Offices {
        private String nameen;

        private String id;

        private String namebg;

        private String is_bc;

        private String longitude;

        private String latitude;

        public String getNameen() {
            return nameen;
        }

        public void setNameen(String nameen) {
            this.nameen = nameen;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNamebg() {
            return namebg;
        }

        public void setNamebg(String namebg) {
            this.namebg = namebg;
        }

        public String getIs_bc() {
            return is_bc;
        }

        public void setIs_bc(String is_bc) {
            this.is_bc = is_bc;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        @Override
        public String toString() {
            return "ClassPojo [nameen = " + nameen + ", id = " + id + ", namebg = " + namebg + ", is_bc = " + is_bc + ", longitude = " + longitude + ", latitude = " + latitude + "]";
        }
    }
}
