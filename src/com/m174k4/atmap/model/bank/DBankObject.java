package com.m174k4.atmap.model.bank;

/*
 * Created by Dimitar Ralev on 17.8.2016 г..
 */
public class DBankObject {
    private Offices[] offices;

    public Offices[] getOffices ()
    {
        return offices;
    }

    public void setOffices (Offices[] offices)
    {
        this.offices = offices;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [offices = "+offices+"]";
    }

    public class Offices
    {
        private String city_id;

        private String city_name;

        private String city_real_id;

        private City_branches[] city_branches;

        public String getCity_id ()
        {
            return city_id;
        }

        public void setCity_id (String city_id)
        {
            this.city_id = city_id;
        }

        public String getCity_name ()
        {
            return city_name;
        }

        public void setCity_name (String city_name)
        {
            this.city_name = city_name;
        }

        public String getCity_real_id ()
        {
            return city_real_id;
        }

        public void setCity_real_id (String city_real_id)
        {
            this.city_real_id = city_real_id;
        }

        public City_branches[] getCity_branches ()
        {
            return city_branches;
        }

        public void setCity_branches (City_branches[] city_branches)
        {
            this.city_branches = city_branches;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [city_id = "+city_id+", city_name = "+city_name+", city_real_id = "+city_real_id+", city_branches = "+city_branches+"]";
        }
    }

    public class City_branches
    {
        private String id;

        private String phone;

        private String email;

        private String address;

        private String location;

        private String name;

        private String pic;

        private String type;

        private String worktime;

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getPhone ()
        {
            return phone;
        }

        public void setPhone (String phone)
        {
            this.phone = phone;
        }

        public String getEmail ()
        {
            return email;
        }

        public void setEmail (String email)
        {
            this.email = email;
        }

        public String getAddress ()
        {
            return address;
        }

        public void setAddress (String address)
        {
            this.address = address;
        }

        public String getLocation ()
        {
            return location;
        }

        public void setLocation (String location)
        {
            this.location = location;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getPic ()
        {
            return pic;
        }

        public void setPic (String pic)
        {
            this.pic = pic;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        public String getWorktime ()
        {
            return worktime;
        }

        public void setWorktime (String worktime)
        {
            this.worktime = worktime;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [id = "+id+", phone = "+phone+", email = "+email+", address = "+address+", location = "+location+", name = "+name+", pic = "+pic+", type = "+type+", worktime = "+worktime+"]";
        }
    }
}
