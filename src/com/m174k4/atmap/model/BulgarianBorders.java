package com.m174k4.atmap.model;

public class BulgarianBorders {
	private final static double LAT_MAX = 44.216352;
	private final static double LAT_MIN = 41.234647;
	private final static double LON_MAX = 28.613937;
	private final static double LON_MIN = 22.359709;

	public static double getLatMax() {
		return LAT_MAX;
	}

	public static double getLatMin() {
		return LAT_MIN;
	}

	public static double getLonMax() {
		return LON_MAX;
	}

	public static double getLonMin() {
		return LON_MIN;
	}
}
