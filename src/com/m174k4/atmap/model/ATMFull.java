package com.m174k4.atmap.model;

/*
 * Created by Dimitar Ralev on 16.8.2016.
 */
public class ATMFull extends ATMLight {
    private String city;
    private String address;
    private int opens;
    private int closes;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getOpens() {
        return opens;
    }

    public void setOpens(int opens) {
        this.opens = opens;
    }

    public int getCloses() {
        return closes;
    }

    public void setCloses(int closes) {
        this.closes = closes;
    }
}
