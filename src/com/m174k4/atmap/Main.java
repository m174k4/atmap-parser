package com.m174k4.atmap;

public class Main {
    public static void main(String[] args) {
        parse();
    }

    private static void parse() {
        Parser parser = new Parser();
        parser.doStuff();
    }
}
