[{
		"Id": 2,
		"TownId": 11,
		"Latitude": 42.0206388889,
		"Longtitude": 23.1002222222,
		"Culture": "bg",
		"Title": "ATM \u0413\u0423\u041c \u0411\u043b\u0430\u0433\u043e\u0435\u0432\u0433\u0440\u0430\u0434",
		"Description": "<p>\u0443\u043b.\"\u0422\u043e\u0434\u043e\u0440 \u0410\u043b\u0435\u043a\u0441\u0430\u043d\u0434\u0440\u043e\u0432\" \u21162<\/p>",
		"Availability": ""
	}, {
		"Id": 3,
		"TownId": 12,
		"Latitude": 42.49749,
		"Longtitude": 27.47287,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u043e\u0444\u0438\u0441 \u0411\u0443\u0440\u0433\u0430\u0441",
		"Description": "<p>\u043f\u043b. \"\u0411\u0430\u0431\u0430 \u0413\u0430\u043d\u043a\u0430\" \u2116 5<\/p>",
		"Availability": ""
	}, {
		"Id": 5,
		"TownId": 13,
		"Latitude": 43.2035555556,
		"Longtitude": 27.9148888889,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u043e\u0444\u0438\u0441 \u0428\u0435\u0439\u043d\u043e\u0432\u043e",
		"Description": "<p>\u0423\u043b. \u0428\u0435\u0439\u043d\u043e\u0432\u043e 2<\/p>",
		"Availability": ""
	}, {
		"Id": 10,
		"TownId": 17,
		"Latitude": 41.5735,
		"Longtitude": 23.727444,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u043e\u0444\u0438\u0441 \u0413\u043e\u0446\u0435 \u0414\u0435\u043b\u0447\u0435\u0432",
		"Description": "<p>\u0443\u043b. \u041c\u0438\u0445\u0430\u0438\u043b \u0410\u043d\u0442\u043e\u043d\u043e\u0432 1<\/p>",
		"Availability": ""
	}, {
		"Id": 11,
		"TownId": 17,
		"Latitude": 41.5716944444,
		"Longtitude": 23.75,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0413\u043e\u0446\u0435 \u0414\u0435\u043b\u0447\u0435\u0432",
		"Description": "<p>\u0443\u043b.\u0414\u0440\u0430\u043c\u0441\u043a\u0438 \u041f\u044a\u0442 32<\/p>",
		"Availability": ""
	}, {
		"Id": 16,
		"TownId": 22,
		"Latitude": 42.279702,
		"Longtitude": 22.692038,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u043e\u0444\u0438\u0441 \u041a\u044e\u0441\u0442\u0435\u043d\u0434\u0438\u043b",
		"Description": "<p>\u0443\u043b. \u0421\u043e\u043b\u0443\u043d 2<\/p>",
		"Availability": ""
	}, {
		"Id": 17,
		"TownId": 23,
		"Latitude": 42.1549166667,
		"Longtitude": 24.7425,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u043e\u0431\u0449\u0438\u043d\u0430 \u041c\u0430\u0440\u0438\u0446\u0430",
		"Description": "<p>\u0431\u0443\u043b. \u041c\u0430\u0440\u0438\u0446\u0430 57<\/p>",
		"Availability": ""
	}, {
		"Id": 18,
		"TownId": 35,
		"Latitude": 42.1874166667,
		"Longtitude": 24.9319722222,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u041c\u0430\u043d\u043e\u043b\u0435",
		"Description": "<p>\u0443\u043b. \u041f\u044a\u0440\u0432\u0430 1<\/p>",
		"Availability": ""
	}, {
		"Id": 19,
		"TownId": 34,
		"Latitude": 42.2108611111,
		"Longtitude": 24.6529722222,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0411\u0435\u043d\u043a\u043e\u0432\u0441\u043a\u0438",
		"Description": "<p>\u0441\u0435\u043b\u043e \u0411\u0435\u043d\u043a\u043e\u0432\u0441\u043a\u0438, \u043d\u0430\u0434 \u043e\u0431\u0449\u0438\u043d\u0430\u0442\u0430<\/p>",
		"Availability": ""
	}, {
		"Id": 20,
		"TownId": 36,
		"Latitude": 42.2370833333,
		"Longtitude": 24.6872222222,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0421\u0442\u0440\u043e\u0435\u0432\u043e",
		"Description": "<p>\u0441. \u0421\u0442\u0440\u043e\u0435\u0432\u043e, \u043d\u0430\u0434 \u043e\u0431\u0449\u0438\u043d\u0430\u0442\u0430<\/p>",
		"Availability": ""
	}, {
		"Id": 22,
		"TownId": 25,
		"Latitude": 43.417216,
		"Longtitude": 24.615257,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u041f\u043b\u0435\u0432\u0435\u043d",
		"Description": "<p>\u0443\u043b. \u0414\u0430\u043d\u0430\u0438\u043b \u041f\u043e\u043f\u043e\u0432 6<\/p>",
		"Availability": ""
	}, {
		"Id": 23,
		"TownId": 26,
		"Latitude": 43.52475,
		"Longtitude": 26.5199166667,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0420\u0430\u0437\u0433\u0440\u0430\u0434",
		"Description": "<p>\u0443\u043b. \u0410\u043f\u0440\u0438\u043b\u0441\u043a\u043e \u0412\u044a\u0441\u0442\u0430\u043d\u0438\u0435 25<\/p>",
		"Availability": ""
	}, {
		"Id": 25,
		"TownId": 27,
		"Latitude": 43.8531944444,
		"Longtitude": 25.95875,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0420\u0443\u0441\u0435",
		"Description": "<p>\u0443\u043b. \u0410\u043b\u0435\u043a\u0441\u0430\u043d\u0434\u0440\u043e\u0432\u0441\u043a\u0430 94<\/p>",
		"Availability": ""
	}, {
		"Id": 27,
		"TownId": 28,
		"Latitude": 42.6792777778,
		"Longtitude": 26.3171111111,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0421\u043b\u0438\u0432\u0435\u043d",
		"Description": "<p>\u0443\u043b. \u0425\u0430\u0434\u0436\u0438 \u0414\u0438\u043c\u0438\u0442\u044a\u0440 13<\/p>",
		"Availability": ""
	}, {
		"Id": 28,
		"TownId": 29,
		"Latitude": 41.5756666667,
		"Longtitude": 24.7076944444,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0421\u043c\u043e\u043b\u044f\u043d",
		"Description": "<p>\u0443\u043b. \u0411\u044a\u043b\u0433\u0430\u0440\u0438\u044f 11<\/p>",
		"Availability": ""
	}, {
		"Id": 30,
		"TownId": 30,
		"Latitude": 42.422488,
		"Longtitude": 25.626726,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0421\u0442\u0430\u0440\u0430 \u0417\u0430\u0433\u043e\u0440\u0430",
		"Description": "<p>\u0443\u043b. \u041c\u0435\u0442\u043e\u0434\u0438\u0439 \u041a\u0443\u0441\u0435\u0432 5<\/p>",
		"Availability": ""
	}, {
		"Id": 31,
		"TownId": 31,
		"Latitude": 41.93382,
		"Longtitude": 25.55549,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0425\u0430\u0441\u043a\u043e\u0432\u043e",
		"Description": "<p>\u0431\u0443\u043b. \u0413.\u0421.\u0420\u0430\u043a\u043e\u0432\u0441\u043a\u0438 8<\/p>",
		"Availability": ""
	}, {
		"Id": 33,
		"TownId": 32,
		"Latitude": 41.7238333333,
		"Longtitude": 24.6848333333,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0427\u0435\u043f\u0435\u043b\u0430\u0440\u0435",
		"Description": "<p>\u0443\u043b. \u0412\u0430\u0441\u0438\u043b \u0414\u0435\u0447\u0435\u0432 42&nbsp;\u0426\u0435\u043d\u0442\u044a\u0440 \u0437\u0430 \u0443\u0441\u043b\u0443\u0433\u0438 \u0438 \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u043d\u0430 \u0433\u0440\u0430\u0436\u0434\u0430\u043d\u0438\u0442\u0435<\/p>",
		"Availability": ""
	}, {
		"Id": 37,
		"TownId": 1,
		"Latitude": 42.7006111111,
		"Longtitude": 23.32125,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0426\u0423",
		"Description": "<p>\u0443\u043b. \u0413\u0435\u043e\u0440\u0433 \u0412\u0430\u0448\u0438\u043d\u0433\u0442\u043e\u043d 21<\/p>",
		"Availability": ""
	}, {
		"Id": 39,
		"TownId": 1,
		"Latitude": 42.6759166667,
		"Longtitude": 23.2868055556,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0414\u043e\u0439\u0440\u0430\u043d",
		"Description": "<p>\u0443\u043b. \u0414\u0435\u0431\u044a\u0440 39<\/p>",
		"Availability": ""
	}, {
		"Id": 40,
		"TownId": 1,
		"Latitude": 42.6648055556,
		"Longtitude": 23.3246388889,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0422\u043e\u043a\u0443\u0434\u0430 \u0411\u043e\u043b\u043d\u0438\u0446\u0430 x2",
		"Description": "<p>\u0431\u0443\u043b. \u041d\u0438\u043a\u043e\u043b\u0430 \u0412\u0430\u043f\u0446\u0430\u0440\u043e\u0432 51\u0411<\/p>",
		"Availability": ""
	}, {
		"Id": 41,
		"TownId": 1,
		"Latitude": 42.661243,
		"Longtitude": 23.351521,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0421\u043e\u0444\u0438\u044f \u042e\u0413",
		"Description": "<p>\u0431\u0443\u043b. \u0413.\u041c.\u0414\u0438\u043c\u0438\u0442\u0440\u043e\u0432, \u0431\u043b. 38<\/p>",
		"Availability": ""
	}, {
		"Id": 42,
		"TownId": 1,
		"Latitude": 42.6795555556,
		"Longtitude": 23.3603888889,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0410\u0433\u0435\u043d\u0446\u0438\u044f\u0442\u0430",
		"Description": "<p>\u0431\u0443\u043b. \u0428\u0438\u043f\u0447\u0435\u043d\u0441\u043a\u0438 \u043f\u0440\u043e\u0445\u043e\u0434 43<\/p>",
		"Availability": ""
	}, {
		"Id": 43,
		"TownId": 16,
		"Latitude": 43.700898,
		"Longtitude": 28.036006,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0413\u0435\u043d\u0435\u0440\u0430\u043b \u0422\u043e\u0448\u0435\u0432\u043e",
		"Description": "<p>\u0443\u043b. \"\u0412\u0430\u0441\u0438\u043b \u0410\u043f\u0440\u0438\u043b\u043e\u0432\" 5, \u0432 \u0441\u0433\u0440\u0430\u0434\u0430\u0442\u0430 \u043d\u0430 \u043e\u0431\u0449\u0438\u043d\u0430 \u0413\u0435\u043d\u0435\u0440\u0430\u043b \u0422\u043e\u0448\u0435\u0432\u043e&nbsp;<\/p>",
		"Availability": ""
	}, {
		"Id": 45,
		"TownId": 38,
		"Latitude": 42.177732,
		"Longtitude": 24.869709,
		"Culture": "bg",
		"Title": "\u0410\u0422\u041c \u0420\u043e\u0433\u043e\u0448",
		"Description": "<p>\u0441\u0435\u043b\u043e \u0420\u043e\u0433\u043e\u0448, \u0412\u0430\u0441\u0438\u043b \u041b\u0435\u0432\u0441\u043a\u0438 61, \u0441\u0433\u0440\u0430\u0434\u0430\u0442\u0430 \u043d\u0430 \u043e\u0431\u0449\u0438\u043d\u0430\u0442\u0430<\/p>",
		"Availability": ""
	}, {
		"Id": 46,
		"TownId": 39,
		"Latitude": 42.61855,
		"Longtitude": 25.39117,
		"Culture": "bg",
		"Title": "ATM \u041a\u0430\u0437\u0430\u043d\u043b\u044a\u043a",
		"Description": "<p>\u0443\u043b. \"\u041f\u0435\u0442\u043a\u043e \u0421\u0442\u0430\u0439\u043d\u043e\u0432\" \u2116 9\u0410<\/p>",
		"Availability": ""
	}, {
		"Id": 47,
		"TownId": 39,
		"Latitude": 42.61044,
		"Longtitude": 25.38707,
		"Culture": "bg",
		"Title": "ATM \u0417\u0430\u0432\u043e\u0434 \u0410\u0440\u0441\u0435\u043d\u0430\u043b",
		"Description": "<p>\u0431\u0443\u043b. \"\u0420\u043e\u0437\u043e\u0432\u0430 \u0414\u043e\u043b\u0438\u043d\u0430\" \u2116100<\/p>",
		"Availability": ""
	}, {
		"Id": 48,
		"TownId": 1,
		"Latitude": 42.698614,
		"Longtitude": 23.333923,
		"Culture": "bg",
		"Title": "ATM \u041e\u0444\u0438\u0441 \u0421\u043e\u0444\u0438\u044f",
		"Description": "<p>\u0431\u0443\u043b. \u041a\u043d\u044f\u0437 \u0410\u043b\u0435\u043a\u0441\u0430\u043d\u0434\u044a\u0440 \u0414\u043e\u043d\u0434\u0443\u043a\u043e\u0432 \u21165<\/p>",
		"Availability": ""
	}, {
		"Id": 49,
		"TownId": 40,
		"Latitude": 43.49625,
		"Longtitude": 24.07652,
		"Culture": "bg",
		"Title": "ATM \u041a\u043d\u0435\u0436\u0430",
		"Description": "<p>\u0443\u043b. \"\u0425\u0440\u0438\u0441\u0442\u043e \u0411\u043e\u0442\u0435\u0432\" \u21162\u0410<\/p>",
		"Availability": ""
	}
]
